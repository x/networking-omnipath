===============================
Welcome to networking-omnipath!
===============================

Summary
-------

OpenStack networking-omnipath (omnipath) is Any ML2 mechanism driver that
integrates OpenStack Neutron API with an Intel Omni-Path Architecture backend.
It enables the Omni-Path switching fabric in OpenStack cloud.
A network in OpenStack networking realm (Neutron) corresponds
to a virtual fabric on Omni-Path side.

To report and discover bugs in networking-omnipath the following
link can be used:
https://launchpad.net/networking-omnipath

Any new code submission or proposal must follow the development
guidelines detailed in HACKING.rst and for further details this
link can be checked:
https://launchpad.net/networking-omnipath

A Python pip package can be found at PyPI:
https://pypi.org/project/networking-omnipath/

Release notes for the project can be found at:
https://pypi.org/project/networking-omnipath/#history

The project source code repository is located at:
https://opendev.org/x/networking-omnipath
